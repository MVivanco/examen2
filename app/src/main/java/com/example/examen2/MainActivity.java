package com.example.examen2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.examen2.database.Producto;
import com.example.examen2.database.ProductosDB;

public class MainActivity extends AppCompatActivity {
    EditText codigoEdit, nombreEdit, marcaEdit, precioEdit;
    Button guardarButton, limpiarButton, nuevoButton, irAButton;
    RadioGroup rg;
    RadioButton perecederoRadio, nomPerecederoRadio;
    private ProductosDB db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new ProductosDB(MainActivity.this);

        codigoEdit = (EditText) findViewById(R.id.codigoMain);
        nombreEdit = (EditText) findViewById(R.id.nombreMain);
        marcaEdit = (EditText) findViewById(R.id.marcaMain);
        precioEdit = (EditText) findViewById(R.id.precioMain);

        guardarButton = (Button) findViewById(R.id.guardarMain);
        limpiarButton = (Button) findViewById(R.id.limpiarMain);
        nuevoButton = (Button) findViewById(R.id.nuevoMain);
        irAButton = (Button) findViewById(R.id.editarMain);

        rg = (RadioGroup) findViewById(R.id.perecederoGroupMain);
        perecederoRadio = (RadioButton) findViewById(R.id.perecederoMain);
        nomPerecederoRadio = (RadioButton) findViewById(R.id.noPerecederoMain);

        guardarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(codigoEdit.getText().toString().matches("") || nombreEdit.getText().toString().matches("")) {
                    Toast.makeText(MainActivity.this, "El campo del codigo y el nombre son obligatorios", Toast.LENGTH_SHORT).show();
                } else {
                    Producto nProducto = new Producto();
                    nProducto.setID(Long.parseLong(codigoEdit.getText().toString()));
                    nProducto.setNombre(nombreEdit.getText().toString());
                    nProducto.setMarca(marcaEdit.getText().toString());
                    nProducto.setPrecio(Float.parseFloat(precioEdit.getText().toString()));
                    nProducto.setPerecedero(perecederoRadio.isChecked() ? 1 : 0);

                    db.openDataBase();

                    if (db.getProducto(nProducto.getID()) != null) {
                        Toast.makeText(MainActivity.this, "El registro " + nProducto.getID() + " ya existe", Toast.LENGTH_SHORT).show();
                    }else{
                        long idx = db.insertarProducto(nProducto);
                        Toast.makeText(MainActivity.this, "Se agregó el nuevo producto", Toast.LENGTH_SHORT).show();
                    }

                    db.cerrarDataBase();
                    limpiar();
                }
            }
        });

        limpiarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        nuevoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        irAButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ProductoActivity.class);
                startActivityForResult(intent, 0);
            }
        });
    }

    private void limpiar() {
        codigoEdit.setText("");
        nombreEdit.setText("");
        marcaEdit.setText("");
        precioEdit.setText("");

        rg.check(R.id.perecederoMain);

    }
}
